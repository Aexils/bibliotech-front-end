import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {ToastController} from '@ionic/angular';
import {AlertController} from '@ionic/angular';

@Component({
    selector: 'app-details',
    templateUrl: './details.page.html',
    styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

    book: any = {};
    dataLoaded: Promise<boolean>;
    idBook = this.activatedRoute.snapshot.paramMap.get('id');
    spinner = true;
    addedB;
    addedW;
    statuses: any = {};
    statusId;
    status;

    constructor(private router: Router,
                private activatedRoute: ActivatedRoute,
                private http: HttpClient,
                private storage: Storage,
                private toast: ToastController,
                private alert: AlertController,
    ) {
    }

    findBook() {
        this.storage.get('jwt').then((data) => {
            const headers = new HttpHeaders({Authorization: 'Bearer ' + data});
            this.http.get('http://127.0.0.1:3000/users/book/library/' + this.idBook, {headers}).subscribe((res) => {
                if (res === 0) {
                    this.addedB = false;
                } else {
                    this.addedB = true;
                }
            });
            this.http.get('http://127.0.0.1:3000/users/book/wish/' + this.idBook, {headers}).subscribe((res) => {
                if (res === 0) {
                    this.addedW = false;
                } else {
                    this.addedW = true;
                }
            });
        });
    }

    addLibrary() {
        const body = {
            idBook: this.idBook
        };
        this.storage.get('jwt').then((data) => {
            const headers = new HttpHeaders({Authorization: 'Bearer ' + data});
            this.http.post('http://127.0.0.1:3000/users/add_book', body, {headers}).subscribe((res) => {
                this.ajouter();
                this.addedB = true;
                this.status = 'Pas commencé';
                this.http.post('http://127.0.0.1:3000/users/bookmark/', body, {headers})
                    .subscribe(() => {
                    });
            }, (err: HttpErrorResponse) => {
                this.errAjouter();
            });
        });
    }

    detailBook() {
        this.http.get('https://www.googleapis.com/books/v1/volumes/' + this.idBook).subscribe((data) => {
            this.book = data;
            this.dataLoaded = Promise.resolve(true);
            this.spinner = false;
            this.storage.get('jwt').then((jwt) => {
                const headers = new HttpHeaders({Authorization: 'Bearer ' + jwt});
                this.http.get('http://127.0.0.1:3000/users/book/status/' + this.idBook, {headers}).subscribe((res) => {
                    // @ts-ignore
                    this.statusId = res.statusId;
                    if (this.statusId === 1) {
                        this.status = 'En cours';
                    } else if (this.statusId === 2) {
                        this.status = 'Terminé';
                    } else {
                        this.status = 'Pas commencé';
                    }
                });
            });
        });
    }

    async ajouter() {
        const toast = await this.toast.create({
            message: 'Livre ajouté à votre bibliothèque',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        await toast.present();
    }

    async errAjouter() {
        const toast = await this.toast.create({
            message: 'Livre déjà ajouté',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        await toast.present();
    }

    addWish() {
        const body = {
            idBook: this.idBook
        };
        this.storage.get('jwt').then((data) => {
            const headers = new HttpHeaders({Authorization: 'Bearer ' + data});
            this.http.post('http://127.0.0.1:3000/users/add_wish', body, {headers}).subscribe((res) => {
                this.ajouterWish();
                this.addedW = true;
            }, (err: HttpErrorResponse) => {
                this.errAjouter();
            });
        });
    }

    async ajouterWish() {
        const toast = await this.toast.create({
            message: 'Livre ajouté à votre liste de souhaits',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        await toast.present();
    }

    async deleteLibrary() {
        const alert = await this.alert.create(
            {
                header: 'Suppression',
                message: 'Êtes-vous sûr de vouloir supprimer ce livre de votre bibliothèque?',
                buttons: [
                    {
                        text: 'Annuler',
                        role: 'cancel',
                        cssClass: 'secondary'
                    }, {
                        text: 'Confirmer',
                        handler: () => {
                            this.storage.get('jwt').then((data) => {
                                const headers = new HttpHeaders({Authorization: 'Bearer ' + data});
                                this.http.delete('http://127.0.0.1:3000/users/book/library/delete/' + this.idBook, {headers})
                                    .subscribe((res) => {
                                        this.toastSuppressionLib();
                                        this.addedB = false;
                                    });
                            });
                        }
                    }
                ]
            });
        await alert.present();
    }

    async toastSuppressionLib() {
        const toast = await this.toast.create({
            message: 'Livre supprimé de votre bibliothèque',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        await toast.present();
    }

    async deleteWish() {
        const alert = await this.alert.create(
            {
                header: 'Suppression',
                message: 'Êtes-vous sûr de vouloir supprimer ce livre de votre liste de souhaits?',
                buttons: [
                    {
                        text: 'Annuler',
                        role: 'cancel',
                        cssClass: 'secondary'
                    }, {
                        text: 'Confirmer',
                        handler: () => {
                            this.storage.get('jwt').then((data) => {
                                const headers = new HttpHeaders({Authorization: 'Bearer ' + data});
                                this.http.delete('http://127.0.0.1:3000/users/book/wish/delete/' + this.idBook, {headers})
                                    .subscribe((res) => {
                                        this.toastSuppressionWish();
                                        this.addedW = false;
                                    });
                            });
                        }
                    }
                ]
            });
        await alert.present();
    }

    async toastSuppressionWish() {
        const toast = await this.toast.create({
            message: 'Livre supprimé de votre liste de souhaits',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        await toast.present();
    }

    async redirectToStore() {
        const url = this.book.saleInfo.buyLink;
        const alert = await this.alert.create(
            {
                header: 'Redirection',
                message: 'Vous allez être rediriger vers le Google Play Store',
                buttons: [
                    {
                        text: 'Annuler',
                        role: 'cancel',
                        cssClass: 'secondary'
                    }, {
                        text: 'Continuer',
                        handler: () => {
                            window.open(url, '_blank');
                        }
                    }
                ]
            });
        await alert.present();
    }

    statusesBook() {
        this.http.get('http://127.0.0.1:3000/statuses').subscribe((res) => {
            this.statuses = res;
        });
    }

    onChange(status) {
        let res = status.target.value;
        if (res === 'En cours') {
            res = 1;
        } else if (res === 'Terminé') {
            res = 2;
            this.storage.get('jwt').then((data) => {
                const headers = new HttpHeaders({Authorization: 'Bearer ' + data});
                const body0 = {idBook: this.idBook, page: this.book.volumeInfo.pageCount};
                this.http.put('http://127.0.0.1:3000/users/bookmark/', body0, {headers})
                    .subscribe(() => {
                    });
            });
        } else if (res === 'Pas commencé') {
            res = 3;
            this.storage.get('jwt').then((data) => {
                const headers = new HttpHeaders({Authorization: 'Bearer ' + data});
                const body0 = {idBook: this.idBook, page: 0};
                this.http.put('http://127.0.0.1:3000/users/bookmark/', body0, {headers})
                    .subscribe(() => {
                    });
            });
        }
        const body = {
            status: res,
            idBook: this.idBook
        };
        this.storage.get('jwt').then((data) => {
            const headers = new HttpHeaders({Authorization: 'Bearer ' + data});
            this.http.put('http://127.0.0.1:3000/users/book/status', body, {headers}).subscribe((response) => {
                this.toastStatus();
            });
        });
    }

    async toastStatus() {
        const toast = await this.toast.create({
            message: 'Statut modifié avec succès !',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        await toast.present();
    }

    ngOnInit() {
        this.findBook();
        this.detailBook();
        this.statusesBook();
    }

}
