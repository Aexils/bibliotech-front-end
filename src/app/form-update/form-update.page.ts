import { Component, OnInit } from '@angular/core';
import {HttpHeaders, HttpClient} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {AlertController} from '@ionic/angular';
import {Router} from '@angular/router';
import {ToastController} from '@ionic/angular';

@Component({
  selector: 'app-form-update',
  templateUrl: './form-update.page.html',
  styleUrls: ['./form-update.page.scss'],
})
export class FormUpdatePage implements OnInit {

  constructor(
      private storage: Storage,
      private http: HttpClient,
      private alert: AlertController,
      private router: Router,
      private toast: ToastController
  ) { }

  email: string;
  pseudo: string;

  data() {
    this.storage.get('pseudo').then((pseudo) => {
      this.pseudo = pseudo;
      console.log(this.pseudo);
    });
    this.storage.get('email').then((email) => {
      this.email = email;
      console.log(this.email);
    });
  }

  async update() {
    const alert = await this.alert.create(
        {
          header: 'Modification',
          message: 'Êtes-vous sûr de vouloir modifier votre profil?',
          buttons: [
            {
              text: 'Annuler',
              role: 'cancel',
              cssClass: 'secondary'
            }, {
              text: 'Confirmer',
              handler: () => {
                const body = {
                  email: this.email,
                  pseudo: this.pseudo
                };
                this.storage.get('jwt').then((data) => {
                  const headers = new HttpHeaders({'Authorization': 'Bearer ' + data});
                  this.http.put('http://127.0.0.1:3000/users/profile', body, {headers}).subscribe(() => {
                    this.router.navigateByUrl('tabs/profil');
                    this.done();
                  });
                });
              }
            }
          ]
        });
    await alert.present();
  }

  async done() {
    const toast = await this.toast.create({
      message: 'Modifications avec succès',
      color: 'primary',
      keyboardClose: true,
      mode: 'ios',
      duration: 2000
    });
    await toast.present();
  }

  ngOnInit() {
    this.data();
  }

}
