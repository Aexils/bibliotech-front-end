import {Component, OnInit} from '@angular/core';
import {Storage} from '@ionic/storage';
import {Router} from '@angular/router';
import {AlertController} from '@ionic/angular';

@Component({
    selector: 'app-profil',
    templateUrl: './profil.page.html',
    styleUrls: ['./profil.page.scss'],
})
export class ProfilPage implements OnInit {

    img;
    pseudo;

    constructor(
        private storage: Storage,
        private router: Router,
        private alert: AlertController
    ) {
        this.storage.get('pseudo').then((pseudo) => {
            this.pseudo = pseudo;
            this.storage.get('img').then((img) => {
                this.img = img;
            });
        });
    }

    async deco() {
        const alert = await this.alert.create(
            {
                header: 'Déconnexion',
                message: 'Êtes-vous sûr de vouloir vous déconnecter?',
                buttons: [
                    {
                        text: 'Annuler',
                        role: 'cancel',
                        cssClass: 'secondary'
                    }, {
                        text: 'Confirmer',
                        handler: () => {
                            this.storage.clear();
                            this.router.navigateByUrl('connexion');
                        }
                    }
                ]
            });

        await alert.present();
    }

    ngOnInit() {
    }

}
