import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'accueil',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../accueil/accueil.module').then(m => m.AccueilPageModule)
          }
        ]
      },
      {
        path: 'bibliotheque',
        children: [
          {
            path: '',
            loadChildren: () =>
                import('../bibliotheque/bibliotheque.module').then(m => m.BibliothequePageModule)
          }
        ]
      },
      {
        path: 'wish',
        children: [
          {
            path: '',
            loadChildren: () =>
                import('../wish/wish.module').then(m => m.WishPageModule)
          }
        ]
      }, {
        path: 'profil',
        children: [
          {
            path: '',
            loadChildren: () =>
                import('../profil/profil.module').then(m => m.ProfilPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/connexion',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/connexion',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
