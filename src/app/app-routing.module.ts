import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'accueil', loadChildren: './accueil/accueil.module#AccueilPageModule' },
  { path: 'details/:id', loadChildren: './details/details.module#DetailsPageModule' },
  { path: 'bibliotheque', loadChildren: './bibliotheque/bibliotheque.module#BibliothequePageModule' },
  { path: 'wish', loadChildren: './wish/wish.module#WishPageModule' },
  { path: 'inscription', loadChildren: './inscription/inscription.module#InscriptionPageModule' },
  { path: 'connexion', loadChildren: './connexion/connexion.module#ConnexionPageModule' },
  { path: 'profil', loadChildren: './profil/profil.module#ProfilPageModule' },
  { path: 'pwd-oublie', loadChildren: './pwd-oublie/pwd-oublie.module#PwdOubliePageModule' },
  { path: 'form-update', loadChildren: './form-update/form-update.module#FormUpdatePageModule' },
  { path: 'details/:id/comment-bookmark', loadChildren: './comment-bookmark/comment-bookmark.module#CommentBookmarkPageModule' }
  ];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
