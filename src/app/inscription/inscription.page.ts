import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {ToastController} from '@ionic/angular';


@Component({
    selector: 'app-inscription',
    templateUrl: './inscription.page.html',
    styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {

    pseudo;
    email;
    password;
    confirmPassword;
    inscription: any = '';
    EMAIL_REGEX = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/;
    PASSWORD_REGEX = /^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,}$/;

    constructor(private router: Router,
                private http: HttpClient,
                private toastController: ToastController) {
    }

    register() {
        const body = {
            "pseudo": this.pseudo,
            "email": this.email,
            "password": this.password
        };

        if (this.pseudo.length >= 15 || this.pseudo.length <= 4) {
            return this.pseudoLength();
        }

        if (!this.EMAIL_REGEX.test(this.email)) {
            return this.testEmail();
        }

        if (!this.PASSWORD_REGEX.test(this.password)) {
            return this.testPwd();
        }

        if (this.password === this.confirmPassword) {
            this.http.post('http://127.0.0.1:3000/users/register/', body).subscribe((data) => {
            });

            this.router.navigateByUrl('connexion');

            this.presentToast();
        } else {
            this.mdpDifferent();
        }
    }

    async presentToast() {
        const toast = await this.toastController.create({
            message: 'Inscription réussie !',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        toast.present();
    }

    async pseudoLength() {
        const toast = await this.toastController.create({
            message: 'Le pseudo doit être entre 5 et 14 caractères',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        toast.present();
    }

    async mdpDifferent() {
        const toast = await this.toastController.create({
            message: 'Le mot de passe n\'est pas le même',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        toast.present();
    }

    async testEmail() {
        const toast = await this.toastController.create({
            message: 'Ceci n\'est pas une adresse email',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        toast.present();
    }

    async testPwd() {
        const toast = await this.toastController.create({
            message: 'Le mot de passe doit contenir au moins 6 caractères, 1 lettre et 1 chiffre',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        toast.present();
    }

    ngOnInit() {
    }

}
