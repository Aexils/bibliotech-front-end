import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-accueil',
    templateUrl: './accueil.page.html',
    styleUrls: ['./accueil.page.scss'],
})
export class AccueilPage implements OnInit {

    books: any = [];
    id: any;
    pseudo;
    trie = 'Titre';
    trie2 = 'Pertinence';

    constructor(private http: HttpClient,
                private route: ActivatedRoute,
                private router: Router,
                private storage: Storage) {
    }

    showDetails(id) {
        this.router.navigate(['details'], id);
        console.log(id);
    }

    search(event) {
        if (this.trie === 'Auteur' && this.trie2 === 'Pertinence') {
            this.http.get<any>('https://www.googleapis.com/books/v1/volumes?q=+inauthor:' + event.detail.value + '&maxResults=20&printType=books&orderBy=relevance').subscribe((data) => {
                this.books = data;
            });
        }

        if (this.trie === 'Auteur' && this.trie2 === 'Nouveauté') {
            this.http.get<any>('https://www.googleapis.com/books/v1/volumes?q=+inauthor:' + event.detail.value + '&maxResults=20&printType=books&orderBy=newest').subscribe((data) => {
                this.books = data;
            });
        }

        if (this.trie === 'Titre' && this.trie2 === 'Pertinence') {
            this.http.get<any>('https://www.googleapis.com/books/v1/volumes?q=+intitle:' + event.detail.value + '&maxResults=20&printType=books&orderBy=relevance&langRestrict=fr').subscribe((data) => {
                this.books = data;
            });
        }

        if (this.trie === 'Titre' && this.trie2 === 'Nouveauté') {
            this.http.get<any>('https://www.googleapis.com/books/v1/volumes?q=+intitle:' + event.detail.value + '&maxResults=20&printType=books&orderBy=newest').subscribe((data) => {
                this.books = data;
            });
        }

        if (this.trie === 'Editeur' && this.trie2 === 'Pertinence') {
            this.http.get<any>('https://www.googleapis.com/books/v1/volumes?q=+inpublisher:' + event.detail.value + '&maxResults=20&printType=books&orderBy=relevance').subscribe((data) => {
                this.books = data;
            });
        }

        if (this.trie === 'Editeur' && this.trie2 === 'Nouveauté') {
            this.http.get<any>('https://www.googleapis.com/books/v1/volumes?q=+inpublisher:' + event.detail.value + '&maxResults=20&printType=books&orderBy=newest').subscribe((data) => {
                this.books = data;
            });
        }
    }

    getPseudo() {
        this.storage.get('pseudo').then((data) => {
            /*const headers = new HttpHeaders({'Authorization': 'Bearer ' + data});
            this.http.get('http://127.0.0.1:3000/users/profile', {headers}).subscribe((res) => {
                this.pseudo = res.user.pseudo;
            });*/
            this.pseudo = data;
        });
    }

    ngOnInit() {
        this.getPseudo();
    }


}
