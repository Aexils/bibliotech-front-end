import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {ToastController} from '@ionic/angular';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-wish',
  templateUrl: './wish.page.html',
  styleUrls: ['./wish.page.scss'],
})
export class WishPage implements OnInit {

  books: any = [];
  dataLoaded: Promise<boolean>;
  idBooks: any = [];
  spinner = true;

  constructor(private router: Router,
              private http: HttpClient,
              private storage: Storage,
              private alert: AlertController,
              private toast: ToastController) {
  }

  displayWish() {
    this.storage.get('jwt').then((token) => {
      const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
      this.http.get<any>('http://127.0.0.1:3000/users/wish', {headers}).subscribe((data) => {
        this.idBooks = data.books;
        /*
                        let livre: any = [];
        */
        for (let i = 0; i < this.idBooks.length; i++) {
          this.http.get<{ books: string[] }>(this.idBooks[i]).subscribe((dataBook) => {
            this.books.push(dataBook);
          });
        }
        this.spinner = false;
      });
    });
  }

  rafraichir(event) {
    setTimeout(() => {
      this.spinner = true;
      this.idBooks = [];
      this.books = [];
      this.displayWish();
      event.target.complete();
    }, 1000);
  }

  async delete(id) {
    const alert = await this.alert.create(
        {
          header: 'Suppression',
          message: 'Êtes-vous sûr de vouloir supprimer ce livre de votre liste de souhaits?',
          buttons: [
            {
              text: 'Annuler',
              role: 'cancel',
              cssClass: 'secondary'
            }, {
              text: 'Confirmer',
              handler: () => {
                this.storage.get('jwt').then((data) => {
                  const headers = new HttpHeaders({'Authorization': 'Bearer ' + data});
                  this.http.delete('http://127.0.0.1:3000/users/book/wish/delete/' + id, {headers})
                      .subscribe((res) => {
                        this.toastSuppressionWish();
                        this.rafraichir(event);
                      });
                });
              }
            }
          ]
        });
    await alert.present();
  }

  async toastSuppressionWish() {
    const toast = await this.toast.create({
      message: 'Livre supprimé de votre liste de souhaits',
      color: 'primary',
      keyboardClose: true,
      mode: 'ios',
      duration: 2000
    });
    await toast.present();
  }

  ngOnInit() {
    this.displayWish();
  }


}
