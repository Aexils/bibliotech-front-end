import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BibliothequePage } from './bibliotheque.page';

describe('BibliothequePage', () => {
  let component: BibliothequePage;
  let fixture: ComponentFixture<BibliothequePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BibliothequePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BibliothequePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
