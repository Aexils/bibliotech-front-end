import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {AlertController} from '@ionic/angular';
import {ToastController} from '@ionic/angular';

@Component({
    selector: 'app-bibliotheque',
    templateUrl: './bibliotheque.page.html',
    styleUrls: ['./bibliotheque.page.scss'],
})
export class BibliothequePage implements OnInit {

    books: any = [];
    dataLoaded: Promise<boolean>;
    idBooks: any = [];
    spinner = true;
    statusId: any = [];
    createdAt: any = [];

    constructor(private router: Router,
                private http: HttpClient,
                private storage: Storage,
                private alert: AlertController,
                private toast: ToastController) {
    }

    displayLibrary() {
        this.storage.get('jwt').then((token) => {
            const headers = new HttpHeaders({Authorization: 'Bearer ' + token});
            this.http.get<any>('http://127.0.0.1:3000/users/books', {headers}).subscribe((data) => {
                this.idBooks = data.books;
                this.statusId = data.resFromDB;
                this.createdAt = data.resFromDB;
                for (let i = 0; i < this.idBooks.length; i++) {
                    this.http.get<{ books: string[] }>(this.idBooks[i]).subscribe((dataBook) => {
                        let status;
                        if (this.statusId[i].statusId === 1) {
                            status = 'En cours';
                        } else if (this.statusId[i].statusId === 2) {
                            status = 'Terminé';
                        } else if (this.statusId[i].statusId === 3) {
                            status = 'Pas commencé';
                        }
                        // @ts-ignore
                        this.http.get<any>('http://127.0.0.1:3000/users/bookmark/' + dataBook.id, {headers}).subscribe((page) => {
                            // @ts-ignore
                            dataBook.page = page;
                        });

                        // @ts-ignore
                        dataBook.createdAt = this.createdAt[i].createdAt;
                        // @ts-ignore
                        dataBook.status = status;
                        this.books.push(dataBook);
                        console.log(this.books);
                    });
                }
                this.spinner = false;
                this.books.sort();
            });
        });
    }

    rafraichir(event) {
        setTimeout(() => {
            this.spinner = true;
            this.idBooks = [];
            this.books = [];
            this.displayLibrary();
            event.target.complete();
        }, 1000);
    }

    async delete(id) {
        const alert = await this.alert.create(
            {
                header: 'Suppression',
                message: 'Êtes-vous sûr de vouloir supprimer ce livre de votre bibliothèque?',
                buttons: [
                    {
                        text: 'Annuler',
                        role: 'cancel',
                        cssClass: 'secondary'
                    }, {
                        text: 'Confirmer',
                        handler: () => {
                            this.storage.get('jwt').then((data) => {
                                const headers = new HttpHeaders({Authorization: 'Bearer ' + data});
                                this.http.delete('http://127.0.0.1:3000/users/book/library/delete/' + id, {headers})
                                    .subscribe((res) => {
                                        this.toastSuppressionLib();
                                        this.rafraichir(event);
                                    });
                            });
                        }
                    }
                ]
            });
        await alert.present();
    }

    async toastSuppressionLib() {
        const toast = await this.toast.create({
            message: 'Livre supprimé de votre bibliothèque',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        await toast.present();
    }

    async bookmark(page, id) {
            const alert = await this.alert.create(
                {
                    header: 'Modifier mon marque-page',
                    inputs: [
                        {
                            name: 'page',
                            placeholder: 'page / ' + page,
                            label: 'page / ' + page,
                            type: 'number',
                            min: 0,
                            max: page
                        }
                    ],
                    buttons: [
                        {
                            text: 'Annuler',
                            role: 'cancel',
                            cssClass: 'secondary'
                        }, {
                            text: 'Confirmer',
                            handler: (pageBook) => {
                                this.storage.get('jwt').then((data) => {
                                    const headers = new HttpHeaders({Authorization: 'Bearer ' + data});
                                    const body = {idBook: id, page: pageBook.page};
                                    this.http.put('http://127.0.0.1:3000/users/bookmark/', body, {headers})
                                        .subscribe((res) => {
                                            this.toastBookmarkUpdate();
                                            this.rafraichir(event);
                                        });
                                });
                            }
                        }
                    ]
                });
            await alert.present();
    }

    async toastBookmarkUpdate() {
        const toast = await this.toast.create({
            message: 'Marque-page modifié !',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        await toast.present();
    }

    ngOnInit() {
        this.displayLibrary();
    }

}
