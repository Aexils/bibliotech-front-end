import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CommentBookmarkPage } from './comment-bookmark.page';

const routes: Routes = [
  {
    path: '',
    component: CommentBookmarkPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [CommentBookmarkPage]
})
export class CommentBookmarkPageModule {}
