import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentBookmarkPage } from './comment-bookmark.page';

describe('CommentBookmarkPage', () => {
  let component: CommentBookmarkPage;
  let fixture: ComponentFixture<CommentBookmarkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentBookmarkPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentBookmarkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
