import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {ToastController} from '@ionic/angular';
import {AlertController} from '@ionic/angular';

@Component({
  selector: 'app-comment-bookmark',
  templateUrl: './comment-bookmark.page.html',
  styleUrls: ['./comment-bookmark.page.scss'],
})
export class CommentBookmarkPage implements OnInit {

  idBook = this.activatedRoute.snapshot.paramMap.get('id');

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private http: HttpClient,
              private storage: Storage,
              private toast: ToastController,
              private alert: AlertController,
  ) {
  }

  ngOnInit() {
  }

}
