import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AlertController} from '@ionic/angular';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Storage} from '@ionic/storage';
import {ToastController} from '@ionic/angular';
import {Facebook} from '@ionic-native/facebook/ngx';
import {Platform} from '@ionic/angular';

@Component({
    selector: 'app-connexion',
    templateUrl: './connexion.page.html',
    styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage implements OnInit {

    email = '';
    password = '';
    isLoggedIn = false;

    constructor(private router: Router,
                private http: HttpClient,
                private  storage: Storage,
                private toast: ToastController,
                private fb: Facebook,
                private platform: Platform
    ) {
    }

    login() {
        const body = {
            'email': this.email,
            'password': this.password
        };
        this.http.post('http://127.0.0.1:3000/users/login/', body).subscribe((data) => {
                this.storage.set('pseudo', data['pseudo']);
                this.storage.set('email', data['email']);
                this.storage.set('jwt', data['token']);
                this.router.navigateByUrl('tabs/accueil');
            },
            (err: HttpErrorResponse) => {
                this.presentToast();
            });
    }

    facebook() {
        if (this.platform.is('cordova')) {
            console.log('platform : cordova');
            this.fbLogin();
        } else {
            console.log('platform : web');
        }
    }

    fbLogin() {
        this.fb.login(['public_profile', 'email'])
            .then(res => {
                if (res.status === 'connected') {
                    this.isLoggedIn = true;
                    this.getUserDetail(res.authResponse.userID);
                } else {
                    this.isLoggedIn = false;
                }
            })
            .catch(e => console.log('Error logging into Facebook', e));
    }

    getUserDetail(userid: any) {
        this.fb.api('/' + userid + '/?fields=id,email,name,picture', ['public_profile'])
            .then(res => {
                console.log(res);
                const body = {
                    email: res.email,
                    pseudo: res.name,
                    idFacebook: res.id
                };
                this.http.post('http://127.0.0.1:3000/users/register_facebook/', body).subscribe((data) => {
                    this.storage.set('pseudo', data['pseudo']);
                    this.storage.set('email', data['email']);
                    this.storage.set('jwt', data['token']);
                    this.storage.set('img', res.picture.data.url);
                    this.router.navigateByUrl('tabs/accueil');
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    async presentToast() {
        const toast = await this.toast.create({
            message: 'Erreur lors de l\'authentification',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        await toast.present();
    }

    ngOnInit() {
        this.storage.get('jwt').then((token) => {
            if (token) {
                this.router.navigateByUrl('tabs/accueil');
            }
        });
    }

}
