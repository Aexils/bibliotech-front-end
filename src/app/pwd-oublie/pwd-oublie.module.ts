import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PwdOubliePage } from './pwd-oublie.page';

const routes: Routes = [
  {
    path: '',
    component: PwdOubliePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PwdOubliePage]
})
export class PwdOubliePageModule {}
