import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {ToastController} from '@ionic/angular';
import {Router} from '@angular/router';

@Component({
    selector: 'app-pwd-oublie',
    templateUrl: './pwd-oublie.page.html',
    styleUrls: ['./pwd-oublie.page.scss'],
})
export class PwdOubliePage implements OnInit {

    emailPwd = '';

    constructor(
        private http: HttpClient,
        private toast: ToastController,
        private router: Router
    ) {
    }

    pwdOublie() {
        const email = {
            'email': this.emailPwd
        };
        this.http.post('http://127.0.0.1:3000/users/forgotten_password', email).subscribe((data) => {
            this.toastEnvoie();
        }, (err: HttpErrorResponse) => {
            this.toastErreur();
        });
    }

    async toastEnvoie() {
        const toast = await this.toast.create({
            message: 'Un mail vous a été envoyé',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        await toast.present();
    }

    async toastErreur() {
        const toast = await this.toast.create({
            message: 'Erreur : email incorrect',
            color: 'primary',
            keyboardClose: true,
            mode: 'ios',
            duration: 2000
        });
        await toast.present();
    }

    ngOnInit() {
    }

}
